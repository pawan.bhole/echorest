from openjdk:8-alpine

ADD /build/libs/gs-rest-service-0.1.0.jar app.jar

ENTRYPOINT ["java", "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8000", "-jar", "/app.jar"]