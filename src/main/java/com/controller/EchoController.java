package com.controller;


import com.model.Echo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EchoController {


    @RequestMapping("/echo")
    public Echo echo(@RequestParam(value = "input") String input) {
        return new Echo(input);
    }
}
