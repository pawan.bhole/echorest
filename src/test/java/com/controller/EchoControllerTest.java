package com.controller;

import com.model.Echo;
import org.junit.Assert;
import org.junit.Test;

public class EchoControllerTest {

    @Test
    public void shouldEchoTheInput() {
        EchoController controller = new EchoController();
        Echo echo = controller.echo("Hello");
        Assert.assertEquals("Hello", echo.getMessage());
    }
}
